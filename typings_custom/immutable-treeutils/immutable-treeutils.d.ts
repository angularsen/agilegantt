import { Seq, List } from 'immutable';
type PathSeq = Seq<string | number, string | number>
/**
 * @id TreeUtils
 * @lookup TreeUtils
 *
 *
 * ### *class* TreeUtils
 *
 * A collection of functional tree traversal helper functions for >ImmutableJS data structures.
 *
 * **Example**
 *
 * ```js
 * const treeUtils = new TreeUtils(Immutable.Seq.of('path', 'to', 'tree'));
 * ```
 *
 * **With custom key accessors**
 *
 * ```js
 * const treeUtils = new TreeUtils(Immutable.Seq.of('path', 'to', 'tree'), '__id', '__children');
 * ```
 *
 * **Note**
 * The first argument of every method of a `TreeUtils` object is the state you want to analyse. I won't mention / explain it again in method descriptions bellow. The argument `idOrKeyPath` also appears in most signatures, its purpose is thoroughly explained in the docs of >byArbitrary.
 *
 *
 * ###### Signature:
 * ```js
 * new TreeUtils(
 *    rootPath?: immutable.Seq,
 *    idKey?: string,
 *    childNodesKey?: string
 * )
 * ```
 *
 * ###### Arguments:
 * * `rootPath` - The path to the substate of your >ImmutableJS state that represents the root node of your tree. Default: `Immutable.Seq()`.
 * * `idKey` - The name of the key that points at unique identifiers of all nodes in your tree . Default: `'id'`.
 * * `childNodesKey` - The name of the key at which child nodes can be found. Default: `'childNodes'`.
 *
 * ###### Returns:
 * * A new `TreeUtils` object
 */
export default class TreeUtils {
    constructor(rootPath?: Seq<{}, {}>, idKey?: string, childNodesKey?: string);
    /**
     * @id TreeUtils-id
     * @lookup id
     *
     * #### *method* id()
     *
     * Returns the id for the node at `keyPath`. Most useful when you want to get the id of the result of a previous tree query:
     * ```js
     * treeUtils.id(state, treeUtils.parent(state, 'node-3'));
     * // 'node-1'
     * ```
     *
     * ###### Signature:
     * ```js
     * id(
     *    state: Immutable.Iterable,
     *    keyPath: Immutable.Seq<string|number>
     * ): string
     * ```
     *
     * ###### Arguments:
     * * `keyPath` - The absolute key path to the substate / node whose id you want to retrieve
     *
     * ###### Returns:
     * The unique identifier of the node at the given key path.
     *
     */
    id(state: any, keyPath: any): any;
    /**
     * @id TreeUtils-nodes
     * @lookup nodes
     *
     * #### *method* nodes()
     *
     * An iterator of all nodes in the tree.
     *
     * ```js
     * for(var nodePath of treeUtils.nodes(state)) {
     *    console.log(treeUtils.id(state, nodePath));
     * }
     * ```
     *
     * ###### Signature:
     * ```
     * nodes(
     *     state: Immutable.Iterable,
     *     path?: Immutable.Seq<string|number>
     * ): Iterator
     * ```
     *
     * ###### Arguments:
     * * `path` - The key path that points at the root of the (sub)tree whose descendants you want to iterate. Default: The `TreeUtils` object's `rootPath`.
     *
     * ###### Returns:
     * An **unordered** [Iterator](https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Iteration_protocols) of all nodes in the tree.
     */
    nodes(state: any, path: any): Iterable<PathSeq>;
    /**
     * @id TreeUtils-find
     * @lookup find
     *
     * #### *method* find()
     *
     * Returns the key path to the first node for which `compatator` returns `true`. Uses >nodes internally and as >nodes is an **unordered** Iterator, you should probably use this to find unique occurences of data.
     * ```js
     * treeUtils.find(state, node => node.get('name') === 'Me in Paris');
     * // Seq ["childNodes", 0, "childNodes", 0]
     * ```
     *
     * ###### Signature:
     * ```js
     * find(
     *    state: Immutable.Iterable,
     *    comparator: Function,
     *    path?: Immutable.Seq<string|number>
     * ): Immutable.Seq<string|number>
     * ```
     *
     * ###### Arguments:
     * * `comparator` - A function that gets passed a node and should return whether it fits the criteria or not.
     * * `path?` - An optional key path to the (sub)state you want to analyse: Default: The `TreeUtils` object's `rootPath`.
     *
     * ###### Returns:
     * The key path to the first node for which `comparator` returned `true`.
     */
    find(state: any, comparator: any, path: any): PathSeq;
    /**
     * @id TreeUtils-filter
     * @lookup filter
     *
     * #### *method* filter()
     *
     * Returns an >Immutable.List of key paths pointing at the nodes for which `comparator` returned `true`.
     * ```js
     * treeUtils.filter(node => node.get('type') === 'folder');
     * //List [ Seq[], Seq["childNodes", 0], Seq["childNodes", 1] ]
     * ```
     *
     * ###### Signature:
     * ```js
     * filter(
     *     state: Immutable.Iterable,
     *     comparator: Function,
     *     path?: Immutable.Seq<string|number>
     * ): List<Immutable.Seq<string|number>>
     * ```
     *
     * ###### Arguments:
     * * `comparator` - A function that gets passed a node and should return whether it fits the criteria or not.
     * * `path?` - An optional key path to the (sub)state you want to analyse: Default: The `TreeUtils` object's `rootPath`.
     *
     *
     * ###### Returns:
     * A >Immutable.List of all the key paths that point at nodes for which `comparator` returned `true`.
     */
    filter(state: any, comparator: any, path: any): List<PathSeq>;
    /**
     * @id TreeUtils-byId
     * @lookup byId
     *
     * #### *method* byId()
     *
     * Returns the key path to the node with id === `id`.
     *
     * ###### Signature:
     * ```js
     * id(
     *    state: Immutable.Iterable,
     *    id: string
     * ): Immutable.Seq<string|number>
     * ```
     *
     * ###### Arguments:
     * * `id` - A unique identifier
     *
     * ###### Returns:
     * The key path to the node with id === `id`.
     */
    byId(state: any, id: any): PathSeq;
    /**
     * @id TreeUtils-byArbitrary
     * @lookup byArbitrary
     *
     * #### *method* byArbitrary()
     *
     * Returns `idOrKeyPath` if it is a >Immutable.Seq, else returns the result of >byId for `idOrKeyPath`. This is used in all other functions that work on a unique identifiers in order to reduce the number of lookup operations.
     *
     * ###### Signature:
     * ```js
     * byArbitrary(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>
     * ): Immutable.Seq<string|number>
     * ```
     * ###### Returns:
     * The key path pointing at the node found for id === `idOrKeyPath` or, if is a >Immutable.Seq, the `idOrKeyPath` itself.
     *
     */
    byArbitrary(state: any, idOrKeyPath: PathSeq): PathSeq;
    /**
     * @id TreeUtils-nextSibling
     * @lookup nextSibling
     *
     * #### *method* nextSibling()
     *
     * ###### Signature:
     * ```js
     * nextSibling(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>
     * ): Immutable.Seq<string|number>
     * ```
     *
     * ###### Returns:
     * Returns the next sibling node of the node at `idOrKeyPath`
     */
    nextSibling(state: any, idOrKeyPath: PathSeq): PathSeq;
    /**
     * @id TreeUtils-previousSibling
     * @lookup previousSibling
     *
     * #### *method* previousSibling()
     *
     * ###### Signature:
     * ```js
     * previousSibling(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>
     * ): Immutable.Seq<string|number>
     * ```
     *
     * ###### Returns:
     * Returns the previous sibling node of the node at `idOrKeyPath`
     */
    previousSibling(state: any, idOrKeyPath: PathSeq): PathSeq;
    /**
     * @id TreeUtils-firstChild
     * @lookup firstChild
     *
     * #### *method* firstChild()
     *
     * ###### Signature:
     * ```js
     * firstChild(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>
     * ): Immutable.Seq<string|number>
     * ```
     *
     * ###### Returns:
     * Returns the first child node of the node at `idOrKeyPath`
     */
    firstChild(state: any, idOrKeyPath: PathSeq): PathSeq;
    /**
     * @id TreeUtils-lastChild
     * @lookup lastChild
     *
     * #### *method* lastChild()
     *
     * ###### Signature:
     * ```js
     * lastChild(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>
     * ): Immutable.Seq<string|number>
     * ```
     *
     * ###### Returns:
     * Returns the last child node of the node at `idOrKeyPath`
     */
    lastChild(state: any, idOrKeyPath: PathSeq): PathSeq;
    /**
     * @id TreeUtils-siblings
     * @lookup siblings
     *
     * #### *method* siblings()
     *
     * ###### Signature:
     * ```js
     * siblings(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>
     * ): Immutable.List<Immutable.Seq<string|number>>
     * ```
     *
     * ###### Returns:
     * Returns a >Immutable.List of key paths pointing at the sibling nodes of the node at `idOrKeyPath`
     */
    siblings(state: any, idOrKeyPath: PathSeq): List<PathSeq>;
    /**
     * @id TreeUtils-childNodes
     * @lookup childNodes
     *
     * #### *method* childNodes()
     *
     * ###### Signature:
     * ```js
     * childNodes(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>
     * ): Immutable.List<Immutable.Seq<string|number>>
     * ```
     *
     * ###### Returns:
     * Returns a >Immutable.List of all child nodes of the node at `idOrKeyPath`
     */
    childNodes(state: any, idOrKeyPath: PathSeq): List<PathSeq>;
    /**
     * @id TreeUtils-childAt
     * @lookup childNodes
     *
     * #### *method* childAt()
     *
     * ###### Signature:
     * ```js
     * childAt(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>,
     *    index: number
     * ): Immutable.Seq<string|number>
     * ```
     *
     * ###### Returns:
     * Returns the child node at position of `index` of the node at `idOrKeyPath`
     */
    childAt(state: any, idOrKeyPath: PathSeq, index: any): PathSeq;
    /**
     * @id TreeUtils-descendants
     * @lookup descendants
     *
     * #### *method* descendants()
     *
     * ###### Signature:
     * ```js
     * descendants(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>,
     * ): Immutable.List<Immutable.Seq<string|number>>
     * ```
     *
     * ###### Returns:
     * Returns a list of key paths pointing at all descendants of the node at `idOrKeyPath`
     */
    descendants(state: any, idOrKeyPath: PathSeq): List<PathSeq>;
    /**
     * @id TreeUtils-childIndex
     * @lookup childIndex
     *
     * #### *method* childIndex()
     *
     * ###### Signature:
     * ```js
     * childIndex(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>,
     * ): number
     * ```
     *
     * ###### Returns:
     * Returns the index at which the node at `idOrKeyPath` is positioned in its parent child nodes list.
     */
    childIndex(state: any, idOrKeyPath: PathSeq): number;
    /**
     * @id TreeUtils-hasChildNodes
     * @lookup hasChildNodes
     *
     * #### *method* hasChildNodes()
     *
     * ###### Signature:
     * ```js
     * hasChildNodes(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>,
     * ): boolean
     * ```
     *
     * ###### Returns:
     * Returns whether the node at `idOrKeyPath` has children.
     */
    hasChildNodes(state: any, idOrKeyPath: PathSeq): boolean;
    /**
     * @id TreeUtils-numChildNodes
     * @lookup numChildNodes
     *
     * #### *method* numChildNodes()
     *
     * ###### Signature:
     * ```js
     * numChildNodes(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>,
     * ): number
     * ```
     *
     * ###### Returns:
     * Returns the number of child nodes the node at `idOrKeyPath` has.
     */
    numChildNodes(state: any, idOrKeyPath: PathSeq): number;
    /**
     * @id TreeUtils-parent
     * @lookup parent
     *
     * #### *method* parent()
     *
     * ###### Signature:
     * ```js
     * parent(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>,
     * ): Immutable.Seq<string|number>
     * ```
     *
     * ###### Returns:
     * Returns the key path to the parent of the node at `idOrKeyPath`.
     */
    parent(state: any, idOrKeyPath: PathSeq): PathSeq;
    /**
     * @id TreeUtils-ancestors
     * @lookup ancestors
     *
     * #### *method* ancestors()
     *
     * ###### Signature:
     * ```js
     * ancestors(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>,
     * ): Immutable.Seq<string|number>
     * ```
     *
     * ###### Returns:
     * Returns an >Immutable.List of all key paths that point at direct ancestors of the node at `idOrKeyPath`.
     */
    ancestors(state: any, idOrKeyPath: PathSeq): List<PathSeq>;
    /**
     * @id TreeUtils-position
     * @lookup position
     *
     * #### *method* position()
     *
     * This method is a very naive attempt to calculate a unqiue numeric position descriptor that can be used to compare two nodes for their absolute position in the tree.
     * ```js
     * treeUtils.position(state, 'node-4') > treeUtils.position(state, 'node-3');
     * // true
     * ```
     *
     * Please note that `position` should not get used to do any comparison with the root node.
     *
     * ###### Signature:
     * ```js
     * position(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>,
     * ): number
     * ```
     *
     * ###### Returns:
     * Returns a unique numeric value that represents the absolute position of the node at `idOrKeyPath`.
     */
    position(state: any, idOrKeyPath: PathSeq): number;
    /**
     * @id TreeUtils-right
     * @lookup right
     *
     * #### *method* right()
     *
     * Returns the key path to the next node to the right. The next right node is either:
     * * The first child node.
     * * The next sibling.
     * * The next sibling of the first ancestor that in fact has a next sibling.
     * * undefined
     *
     * ```js
     * let nodePath = treeUtils.byId(state, 'root');
     * while (nodePath) {
     *    console.log(nodePath);
     *    nodePath = treeUtils.right(state, nodePath);
     * }
     * // 'root'
     * // 'node-1'
     * // 'node-2'
     * // 'node-3'
     * // 'node-4'
     * // 'node-5'
     * // 'node-6'
     * ```
     *
     * ###### Signature:
     * ```js
     * right(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>,
     * ): Immutable.Seq<string|number>
     * ```
     *
     * ###### Returns:
     * Returns the key path to the node to the right of the one at `idOrKeyPath`.
     */
    right(state: any, idOrKeyPath: PathSeq): PathSeq;
    /**
     * @id TreeUtils-left
     * @lookup left
     *
     * #### *method* left()
     *
     * Returns the key path to the next node to the left. The next left node is either:
     * * The last descendant of the previous sibling node.
     * * The previous sibling node.
     * * The parent node.
     * * undefined
     *
     * ```js
     * let nodePath = treeUtils.lastDescendant(state, 'root');
     * while (nodePath) {
     *    console.log(nodePath);
     *    nodePath = treeUtils.left(state, nodePath);
     * }
     * // 'node-6'
     * // 'node-5'
     * // 'node-4'
     * // 'node-3'
     * // 'node-2'
     * // 'node-1'
     * // 'root'
     * ```
     *
     *
     * ###### Signature:
     * ```js
     * left(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>,
     * ): Immutable.Seq<string|number>
     * ```
     *
     * ###### Returns:
     * Returns the key path to the node to the right of the one at `idOrKeyPath`.
     */
    left(state: any, idOrKeyPath: PathSeq): PathSeq;
    /**
     * @id TreeUtils-firstDescendant
     * @lookup firstDescendant
     *
     * #### *method* firstDescendant()
     *
     * Alias of >firstChild.
     */
    firstDescendant(state: any, idOrKeyPath: PathSeq): PathSeq;
    /**
     * @id TreeUtils-lastDescendant
     * @lookup lastDescendant
     *
     * #### *method* lastDescendant()
     *
     * Returns the key path to the most right node in the given subtree (keypath). The last child of the most deep descendant, if that makes any sense. Look at the example:
     *
     * ```js
     * treeUtils.lastDescendant(state, 'root');
     * // 'node-6'
     * ```
     *
     * ###### Signature:
     * ```js
     * lastDescendant(
     *    state: Immutable.Iterable,
     *    idOrKeyPath: string|Immutable.Seq<string|number>,
     * ): Immutable.Seq<string|number>
     * ```
     *
     * ###### Returns:
     * Returns the key path to the last descendant of the node at `idOrKeyPath`.
     */
    lastDescendant(state: any, idOrKeyPath: PathSeq): PathSeq;
}