# AgileGantt
Gantt for agile teams, where the roadmap and priorities changes frequently and you need to quickly figure out how to best employ your resources.

Based on [vortigern](https://github.com/barbar/vortigern) starter-kit, modified for static builds.

## Installing

```bash
# Install pre-requisites
$ npm i -g yarn     # Install Yarn package manager, faster than npm

$ git clone https://bitbucket.org/anjdreas/agilegantt.git
$ cd agilegantt
$ yarn install      # Install NPM packages including TypeScript typings
```

## Using

All commands defaults to development environment unless otherwise specified. You can set `NODE_ENV` to `production` or use the custom tasks below.

```bash
# Run development build with hot-reloading
$ yarn start

# Run production build (minified bundles, optimized)
$ yarn run start:prod

# Building
$ yarn build          # Development build
$ yarn run build:prod # Production build

# Testing
$ yarn test
```

## Core Dependencies
- [TypeScript](https://www.typescriptlang.org/) for a type system transpiled to javascript.
- [React](https://github.com/facebook/react) for views.
- [Redux](https://github.com/reactjs/redux) for managing application state.

#### Build System
- [Webpack](https://github.com/webpack/webpack) for bundling.
  - [TypeScript Loader](https://github.com/andreypopp/typescript-loader) as ts loader.
  - [Babel Loader](https://github.com/babel/babel-loader) as js loader.
  - [React Hot Loader](https://github.com/gaearon/react-hot-loader) for providing hot reload capability to our development server

#### Dev & Prod Server
- [Webpack Dev Server](https://github.com/webpack/webpack-dev-server)
  - [Webpack Dev Middleware](https://github.com/webpack/webpack-dev-middleware)
  - [Webpack Hot Middleware](https://github.com/webpack/webpack-hot-middleware)
- [Express](https://github.com/expressjs/express) for running server both on client and server side.
- [Compression](https://github.com/expressjs/compression) for gzip compression
- [Serve Favicon](https://github.com/expressjs/serve-favicon) for serving favicon.

#### Developer Experience
- [Typings](https://github.com/typings/typings) for installing type definitions of external libraries.
- [tslint](https://github.com/palantir/tslint) for linting TypeScript files.
- [stylelint](https://github.com/stylelint/stylelint) for linting styles.
- [Redux Logger](https://github.com/theaqua/redux-logger)
- [Redux DevTools](https://github.com/gaearon/redux-devtools)
- [Chalk](https://github.com/chalk/chalk) for colored terminal logs.

#### Testing
- [Karma](https://github.com/karma-runner/karma) as test runner with following plugins
  - [Karma-Webpack](https://github.com/webpack/karma-webpack)
  - [Karma-Mocha](https://github.com/karma-runner/karma-mocha)
  - [Karma-Chai](https://github.com/xdissent/karma-chai)
  - [Karma-Coverage](https://github.com/karma-runner/karma-coverage)
  - [Karma-PhantomJS Launcher](https://github.com/karma-runner/karma-phantomjs-launcher)
- [Mocha](https://github.com/mochajs/mocha) as testing framework.
- [Chai](https://github.com/chaijs/chai) as assertion library.
- [Enzyme](https://github.com/jquery/jquery) for rendering React Components.
- [Fetch Mock](https://github.com/wheresrhys/fetch-mock) for testing async actions.
- [Redux Mock Store](https://github.com/arnaudbenard/redux-mock-store) for creating mock stores.

## Directory Structure
```bash
.
├── build                       # Built, ready to serve app.
├── config                      # Root folder for configurations.
│   ├── test                    # Test configurations.
│   ├── types                   # Global type definitions, written by us.
│   ├── webpack                 # Webpack configurations.
│   └── main.ts                 # Generic App configurations.
├── node_modules                # Node Packages.
├── src                         # Source code.
│   ├── app                     # App folder.
│   │ ├── components            # React Components.
│   │ ├── containers            # React/Redux Containers.
│   │ ├── helpers               # Helper Functions & Components.
│   │ ├── redux                 # Redux related code aka data layer of the app.
│   │ │   ├── modules           # Redux modules.
│   │ │   ├── reducers.ts       # Main reducers file to combine them.
│   │ │   └── store.ts          # Redux store, contains global app state.
│   │ └── routes.tsx            # Routes.
│   ├── client.tsx              # Entry point for client side rendering.
│   └── server.tsx              # Entry point for server side rendering.
├── typings                     # Type definitions installed with typings.
├── .dockerignore               # Tells docker which files to ignore.
├── .gitignore                  # Tells git which files to ignore.
├── .stylelintrc                # Configures stylelint.
├── Dockerfile                  # Dockerfile.
├── package.json                # Package configuration.
├── README.md                   # This file
├── tsconfig.json               # TypeScript transpiler configuration.
├── tslint.json                 # Configures tslint.
└── typings.json                # Typings package configuration.
```