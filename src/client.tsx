import 'isomorphic-fetch';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { fromJS } from 'immutable';
// import { ReduxAsyncConnect } from 'redux-connect';

import { configureStore } from './app/redux/store';
import { IStore } from './app/redux/IStore';
import 'isomorphic-fetch';
import routes from './app/routes';
import { Storage } from './app/helpers/StoreStorageProviderLocalStorage';

const appElement = document.getElementById('app');
const token = getStoreToken();
const storage = new Storage({ token });

getOrCreateInitialState(storage)
  .then((state) => {
    const store = configureStore(hashHistory, state);
    storage.bind(store);

    // any: Element not assignable to RouterContext
    // const connectedCmp = (props) => <ReduxAsyncConnect {...props} /> as any;
    const history = syncHistoryWithStore(hashHistory, store, {
      // Convert immutable react-router-redux state to JS object
      selectLocationState(state) {
        return state.get('routing').toJS();
      },
    });

    ReactDOM.render((
      <Provider store={store} key="provider">
        <Router history={history} /*render={connectedCmp} */ >
          {routes}
        </Router>
      </Provider>
    ), appElement);
  })
  .catch((err) => {
    console.error('Error loading.', err);

    ReactDOM.render((
      <div>Error loading: {err.toString()}</div>
    ), appElement);
  },
);

function getURLParameter(name): string {
  const pattern = '[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)';
  const value = decodeURIComponent(
    (new RegExp(pattern).exec(location.hash) || [undefined, ''])
    [1].replace(/\+/g, '%20'),
  );

  return (value && value.length > 0) ? value : undefined;
}

function getOrCreateInitialState(store: Storage): Promise<IStore> {
  return store.load()
    .then((state) => state || fromJS({}) as IStore);
}

function getStoreToken() {
  let token = getURLParameter('storeToken');
  if (token) {
    console.debug('Using store token from URL: ' + token);
    localStorage.setItem('storeToken', token);
    location.href = location.href.replace(/[?&]storeToken=[\w]+\b/, '');
    return token;
  }

  token = localStorage.getItem('storeToken');
  if (token) {
    console.debug('Using store token from localStorage: ' + token);
    return token;
  }
  return undefined;
}
