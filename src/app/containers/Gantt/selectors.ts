import { createSelector } from 'reselect';
import { List, Seq } from 'immutable';

import { NAME } from './constants';
import { IModel, ITaskJs, ITask } from './model';
import { default as NodeUtils } from '../../utils/NodeUtils';

// Direct selectors
const getModel = (state: any) => state.get(NAME) as IModel;

export const getTasksPreOrderedFromRoot =
  (root: ITask) => List<ITask>(NodeUtils.traverseDfsPreOrder(root, ((task) => task.get('children').toArray())));

export const getSelectedTaskIdFromModel =
  (model: IModel) => {

    const selectedTaskPath = model.get('selectedTaskPath');
    if (selectedTaskPath === undefined) {
      return undefined;
    }
    return model.getIn(Seq(['root', selectedTaskPath, 'id'])) as number;
  };

// export const getTotalWork = (() => {
//   let taskRefToWork = Map<ITaskJs, number>();
//   function f(task: ITaskJs) {
//     if (taskRefToWork.has(task)) {
//       return taskRefToWork.get(task);
//     }

//     let work: number = task.children === undefined || task.children.length === 0
//       ? task.work !== undefined ? task.work : 0
//       : task.children.reduce((prev, curr) => prev + f(curr), 0);

//     taskRefToWork = taskRefToWork.set(task, work);
//     return work;
//   }
//   return f;
// })();

// Memoized selectors (reselect)
export const getRoot = createSelector(getModel, (model) => model.get('root'));
export const getTasksPreOrdered = createSelector(getRoot, (root) => getTasksPreOrderedFromRoot(root));
export const getSelectedTaskPath = createSelector(getModel, (model) => model.get('selectedTaskPath'));
export const getIsEditing = createSelector(getModel, (model) => model.get('isEditing'));

export const getSelectedTask = createSelector(getRoot, getSelectedTaskPath,
  (root, selectedTaskPath) => root.getIn(selectedTaskPath).toJS() as ITaskJs);

export const getSelectedTaskId = createSelector(getRoot, getSelectedTaskPath,
  (root, selectedTaskPath) => {
    if (selectedTaskPath === undefined) {
      return undefined;
    }
    return root.getIn(selectedTaskPath.concat('id')) as number;
  });

export const getSelectedColName = createSelector(getModel, (model) => model.get('selectedColName'));
export const getCols = createSelector(getModel, (model) => model.get('cols'));
