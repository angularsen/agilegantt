import { fromJS, Seq, Map, List } from 'immutable';
import * as moment from 'moment';
import TreeUtils from 'immutable-treeutils';

type PathSeq = Seq<string | number, string | number>;

import { IModel, IModelJs, ITaskJs, IColumnJs, ITask, IIsEditingJs } from './model';
import * as t from './actionTypes';
import * as selectors from './selectors';

// import { root as initialRoot } from './db';
import NodeUtils from '../../utils/NodeUtils';

const treeUtils = new TreeUtils(Seq(), 'id', 'children');
type Moment = moment.Moment;
type Duration = moment.Duration;

// TODO Make configurable
interface IWorkDay {
  /**
   * Time of day when work day starts.
   */
  start: Duration;
  /**
   * Time of day when work day ends.
   */
  end: Duration;
}

/**
 * Path used as fallback to fix invalid selected paths, such as non-existing tasks.
 */
const pathToFirstTask = Seq(['children', 0]);

const config = {
  workDay: <IWorkDay> {
    start: moment.duration(8, 'hours'),
    end: moment.duration(16, 'hours'),
  },
};

const initialState: IModel = fromJS({
  root: <ITaskJs> { id: 1, title: '(root)', children: [] }, // initialRoot,
  selectedTaskPath: ['children', 0],
  selectedColName: 'Title',
  cols: fromJS([
    { name: 'Wbs', propName: 'wbs', isReadOnly: true, type: 'string' },
    { name: 'Title', propName: 'title', type: 'string' },
    { name: 'Work', propName: 'work', type: 'number' },
    { name: 'Resources', propName: 'resources', type: 'string' },
  ] as IColumnJs[]),
  isEditing: undefined,
} as IModelJs);

export const reducer = (state = initialState, action: any): IModel => {
  let root = state.get('root');

  if (!state.get('isHydrated')) {
    // Merge state with initialState, when hydrating a partially stored state to local storage
    state = initialState.merge(state).set('hydrated', true);
    // Validate
    const rootJs = root.toJS();
    NodeUtils.traverseDfsPreOrder(rootJs, (t) => t.children).forEach(validateTask);

    root = updateDerivedValues(root);
    state = state.update('root', (x) => root);
  }

  const cols = state.get('cols');
  const selectedColName = state.get('selectedColName');
  const selectedTaskPath: PathSeq | undefined = Seq(state.get('selectedTaskPath'));

  switch (action.type) {
    case t.NAV_UP: {
      if (selectedTaskPath === undefined) { break; }
      const nextPath = treeUtils.left(root, selectedTaskPath);
      const nextId = treeUtils.id(root, nextPath) as number;
      return nextPath && nextId !== root.get('id')
        ? state.update('selectedTaskPath', (x) => nextPath)
        : state;
    }

    case t.NAV_DOWN: {
      if (selectedTaskPath === undefined) { break; }
      const nextPath = treeUtils.right(root, selectedTaskPath);
      return nextPath
        ? state.update('selectedTaskPath', (x) => nextPath)
        : state;
    }

    case t.NAV_LEFT: {
      if (selectedTaskPath === undefined) { break; }
      const colIdx = cols.findIndex((c) => c.get('name') === selectedColName);
      const newColIdx = colIdx - 1;

      return newColIdx >= 0
        ? state.update('selectedColName', (x) => cols.get(newColIdx).get('name'))
        : state;
    }

    case t.NAV_RIGHT: {
      if (selectedTaskPath === undefined) { break; }
      const colIdx = cols.findIndex((c) => c.get('name') === selectedColName);
      const newColIdx = colIdx + 1;

      return newColIdx < cols.count()
        ? state.update('selectedColName', (x) => cols.get(newColIdx).get('name'))
        : state;
    }

    case t.MOVE_UP: {
      if (selectedTaskPath === undefined) { break; }
      return moveChildIndex(state, root, selectedTaskPath, -1);
    }

    case t.MOVE_DOWN: {
      if (selectedTaskPath === undefined) { break; }
      return moveChildIndex(state, root, selectedTaskPath, +1);
    }

    case t.MOVE_RIGHT: {
      if (selectedTaskPath === undefined) { break; }
      const task = root.getIn(selectedTaskPath);
      const prevSiblingPath = treeUtils.previousSibling(root, selectedTaskPath);
      if (!prevSiblingPath) {
        return state;
      }

      root = root
        .deleteIn(selectedTaskPath)
        .updateIn(prevSiblingPath.concat('children'), (children: any[]) => children.push(task));

      root = updateDerivedValues(root);

      const newPath = treeUtils.lastChild(root, prevSiblingPath);
      return state
        .update('root', (x) => root)
        .update('selectedTaskPath', (x) => newPath);
    }

    case t.MOVE_LEFT: {
      if (selectedTaskPath === undefined) { break; }
      const task = root.getIn(selectedTaskPath);
      const parentPath = treeUtils.parent(root, selectedTaskPath);
      const grandParentPath = treeUtils.parent(root, parentPath);
      if (!parentPath || !grandParentPath) {
        return state;
      }

      const parentChildIdx = treeUtils.childIndex(root, parentPath);
      const newPath = grandParentPath.concat(['children', parentChildIdx + 1]);

      root = root
        .deleteIn(selectedTaskPath)
        .updateIn(grandParentPath.concat('children'),
        (children: List<any>) => children.splice(parentChildIdx + 1, 0, task));

      root = updateDerivedValues(root);
      return state
        .update('root', (x) => root)
        .update('selectedTaskPath', (x) => newPath);
    }

    case t.SET_PROPERTY: {
      const payload = action.payload as t.ISetPropertyPayload;
      const col = cols.find((c) => c.get('name') === selectedColName);
      const propName = col.get('propName');
      const type = col.get('type');

      let value: string | number;
      switch (type) {
        case 'number':
          value = parseFloat(payload.value);
          break;
        case 'string':
          value = payload.value;
          break;
        default:
          throw new Error('Not implemented: ' + type);
      }

      root = root.updateIn(selectedTaskPath.concat(propName), () => value);
      root = updateDerivedValues(root);

      return state
        .update('root', (x) => root)
        .delete('isEditing');
    }

    case t.EDIT_PROPERTY: {
      const opts = action.payload as t.IEditPropertyPayload;
      return state
        .set('isEditing', Map({
          taskId: selectors.getSelectedTaskIdFromModel(state),
          colName: selectedColName,
          initialCursorPosition: opts.cursorPosition,
        } as IIsEditingJs));
    }

    case t.CANCEL_EDIT: {
      return state.delete('isEditing');
    }

    case t.ADD_BEFORE: {
      const validPath = isSelectedTaskPathValid(selectedTaskPath, root);
      let insertPath: PathSeq = validPath
        ? selectedTaskPath
        : pathToFirstTask;

      const newTask: ITaskJs = getNewTask(root);
      root = insertTask(root, newTask, insertPath);

      return state
        .update('root', (x) => root)
        .update('selectedTaskPath', (x) => insertPath)
        .set('isEditing', Map({ taskId: newTask.id, colName: selectedColName }))
        .update('selectedColName', (x) => 'Title');
    }

    case t.ADD_AFTER: {
      const validPath = isSelectedTaskPathValid(selectedTaskPath, root);
      const insertPath: PathSeq = validPath
        ? treeUtils.nextSibling(root, selectedTaskPath) || selectedTaskPath
        : pathToFirstTask;

      const newTask: ITaskJs = getNewTask(root);
      root = insertTask(root, newTask, insertPath);

      return state
        .update('root', (x) => root)
        .update('selectedTaskPath', (x) => insertPath)
        .set('isEditing', Map({ taskId: newTask.id, colName: selectedColName }))
        .update('selectedColName', (x) => 'Title');
    }

    case t.REMOVE: {
      const prevPath = treeUtils.left(root, selectedTaskPath);

      root = root.deleteIn(selectedTaskPath);
      root = updateDerivedValues(root);

      return state
        .update('root', (x) => root)
        .update('selectedTaskPath', (x) => prevPath);
    }
    default: {
      console.log('Unknown action', action);
      return state;
    }
  }
};

function insertTask(root: ITask, taskJs: ITaskJs, path: PathSeq | undefined): ITask {
  const task = fromJS(taskJs);

  // [children, 5] => [children]
  const childrenPath = path.butLast();
  const childIdx = treeUtils.childIndex(root, path);

  root = root.updateIn(
    childrenPath,
    (children: List<ITask>) => children.splice(childIdx, 0, task));

  root = updateDerivedValues(root);
  return root;
}

function getNewTask(root: ITask): ITaskJs {
  const rootJs = root.toJS();
  const maxId = Math.max(...NodeUtils.traverseDfsPostOrder(rootJs, (t) => t.children).map((t) => t.id));
  if (maxId === undefined) {
    throw Error('Error finding next available ID.');
  }

  return {
    id: maxId + 1,
    title: 'New task',
    children: [],
    wbs: null,
    depth: 0,
    work: 0,
    start: null,
    end: null,
    resources: null,
  };
}

/**
 * Sets moment to the time of day when the work day starts.
 * @param moment Moment to manipulate.
 * @param hours Start of work day hours, defaults to 8.
 * @param minutes Start of work day minutes, defaults to 0.
 */
function setToStartOfWorkDay(
  m: Moment,
  startTimeOfDay = config.workDay.start): Moment {
  return m.hours(startTimeOfDay.hours()).minutes(startTimeOfDay.minutes());
}

/**
 * Sets moment to the time of day when the work day ends.
 * @param moment Moment to manipulate.
 * @param hours End of work day hours, defaults to 16.
 * @param minutes End of work day minutes, defaults to 0.
 */
function setToEndOfWorkDay(
  m: Moment,
  endTimeOfDay = config.workDay.end): Moment {
  return m.hours(endTimeOfDay.hours()).minutes(endTimeOfDay.minutes());
}

function getTimeLeftOfWorkDay(m: Moment, workDay: IWorkDay = config.workDay): Duration {

  // TODO Configurable holidays, weekends, custom vacation days per person etc
  const isSunday = m.weekday() === 0;
  const isSaturday = m.weekday() === 6;
  if (isSaturday || isSunday) {
    return moment.duration();
  }

  const workDayStart = setToStartOfWorkDay(moment(m));
  const workDayEnd = setToEndOfWorkDay(moment(m));
  if (m.isBefore(workDayStart) || m.isAfter(workDayEnd)) {
    return moment.duration();
  }

  return moment.duration(workDayEnd.diff(m));
}

// TODO Optimize this by creating a function for only updating ancestors,
// as that is the typical case when modifying/moving a task, instead
// of updating the entire tree as here.
function updateDerivedValues(root: ITask) {
  let resourceToLastWorkEnd = Map<string, Moment>();
  const rootJs = root.toJS();
  const rootStart = moment(rootJs.start);

  const processTask = (t: ITaskJs) => {
    const iterPath = treeUtils.byId(root, t.id);
    const ancestors = treeUtils.ancestors(root, iterPath);
    const depth = ancestors.count() - 1;
    const nodePaths = List<PathSeq>([iterPath]).concat(ancestors).skipLast(1).reverse().toList();
    const wbs = nodePaths.map((ancestorPath) => treeUtils.childIndex(root, ancestorPath) + 1).toArray().join('.');

    t.wbs = wbs;
    t.depth = depth;

    const childTasks = t.children;

    if (t.children === undefined || t.children.length === 0) {
      // Calculate work period of task.
      // Allocate its work period and update the task values.
      const start = resourceToLastWorkEnd.get(t.resources) || rootStart || setToStartOfWorkDay(moment());
      const allocatedWorkPeriod = allocateWorkPeriod(start, t.work);

      t.start = allocatedWorkPeriod.start.toDate();
      t.end = allocatedWorkPeriod.end.toDate();
      t.work = parseFloat(t.work as any);
      resourceToLastWorkEnd = resourceToLastWorkEnd.set(t.resources, moment(t.end));
    } else {
      // Calculate start/end dates and total work of parents by their children
      t.work = List(childTasks)
        .map((t) => t.work)
        .reduce((prev, next) => parseFloat(prev as any) + parseFloat(next as any), 0);

      t.start = List(childTasks.map((t) => t.start)).min();
      t.end = List(childTasks.map((t) => t.end)).max();
    }

    // Update state with new task values
    root = root.setIn(iterPath, fromJS(t));
  };

  // Traverse tasks post-order (all children before its parent)
  NodeUtils.traverseDfsPostOrder(rootJs, (t) => t.children)
    .forEach(processTask);

  return root;
}

interface IWorkPeriod {
  start: Moment;
  end: Moment;
}

function hasTime(d: Duration): boolean {
  return d.asMilliseconds() > 0;
}

function allocateWorkPeriod(start: Moment, workHours: number): IWorkPeriod {
  const maxDays = 1000;
  let timeCursor = moment(start);
  let workLeft = moment.duration(workHours, 'hours');

  for (let dayCount = 0; dayCount < maxDays && hasTime(workLeft); dayCount++) {
    const timeLeftOfWorkDay = getTimeLeftOfWorkDay(timeCursor);

    if (hasTime(timeLeftOfWorkDay)) {
      const timeToSpend = moment.duration(workLeft < timeLeftOfWorkDay ? workLeft : timeLeftOfWorkDay);
      workLeft = workLeft.subtract(timeToSpend);
      timeCursor = timeCursor.add(timeToSpend);
    }
    if (hasTime(workLeft)) {
      // If still remaining work, move to start of next work day
      timeCursor = timeCursor.add(1, 'day');
      timeCursor = setToStartOfWorkDay(timeCursor);
    }
  }

  return {
    start,
    end: timeCursor,
  };
}

function moveChildIndex(model: IModel, root: ITask, path: PathSeq, childIdxOffset: number) {
  const childIdx = treeUtils.childIndex(root, path);
  const newChildIdx = childIdx + childIdxOffset;

  const parentPath = treeUtils.parent(root, path);
  const childCount = treeUtils.numChildNodes(root, parentPath);

  if (newChildIdx < 0 || newChildIdx >= childCount) {
    return model;
  }

  const newPath = treeUtils.childAt(root, parentPath, newChildIdx);

  root = root
    .updateIn(parentPath.concat('children'), (children: List<ITaskJs>) => moveElement(children, childIdx, newChildIdx));

  return model
    .update('root', (x) => root)
    .update('selectedTaskPath', (x) => newPath);
}

function moveElement<T>(list: List<T>, from: number, to: number): List<T> {
  const oldItem = list.get(from);
  return list.delete(from).insert(to, oldItem);
}

/**
 * Validates the properties of the task. Does not recursively validate the `children` property.
 * @param task
 */
function validateTask(task: ITaskJs) {
  if (task === undefined) {
    throw new Error('Undefined task.');
  }
  if (typeof task.id !== 'number') {
    throw new Error('Invalid property `id`: ' + task.id);
  }
  if (task.children === undefined || task.children.constructor !== Array) {
    throw new Error('Invalid property `children`: ' + task.children);
  }
  task.work = parseFloat(task.work as any);
};

function isSelectedTaskPathValid(selectedTaskPath: PathSeq, root: ITask): boolean {
  return selectedTaskPath === undefined ||
    !root.hasIn(selectedTaskPath) ||
    selectedTaskPath.count() > 0;
}
