import * as React from 'react';
// import * as ReactDOM from 'react-dom';
import { List } from 'immutable';
import * as moment from 'moment';
import * as _ from 'underscore';

import {ITask} from '../model';
import * as selectors from '../selectors';

let {connect} = require('react-redux');

// declare var google: any;

function stringToColor(str: string) {

    // str to hash
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
        // tslint:disable-next-line:no-bitwise
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }

    // int/hash to hex
    let color = '#';
    for (let i = 0; i < 3; i++) {
        // tslint:disable-next-line:no-bitwise
        color += ('00' + ((hash >> i * 8) & 0xFF).toString(16)).slice(-2);
    }

    return color;
}

export interface IGanttRow {
    taskId: string;
    taskName: string;
    resourceId?: string;
    start?: Date;
    end?: Date;
    percentComplete?: number;
}

export interface IStateProps {
    tasks: List<ITask>;
}

export interface IDispatchProps {
}

export interface IGanttChartProps extends IStateProps, IDispatchProps {
}

export class GanttChart extends React.Component<IGanttChartProps, any> {
    constructor(props: IGanttChartProps) {
        super(props);
    }

    public render() {
        const {tasks} = this.props;
        const rows = tasks.skip(1).map(this.getGanttRow).toArray(); // skip root

        // Start on first weekday of first week of tasks, makes rendering nicer and easier showing only whole weeks
        const start = moment(_.min(rows, (r) => r.start).start).weekday(1); // Monday
        const end = moment(_.max(rows, (r) => r.end).end);
        const diffDays = moment.duration(end.diff(start)).asDays();
        const days = _.range(0, diffDays - 1).map((day) => moment(start).add(day, 'day'));
        const firstDayOfEachWeek = _.map(_.uniq(days, (d) => d.week()), (d) => moment(d).weekday(1));

        const getWeekHeader = (day: moment.Moment) => (
            <th key={day.toISOString()} colSpan={7}> {`Week ${day.week()}, ${day.year()}`} </th>
        );

        const getDateHeader = (day: moment.Moment) => (
            <th key={day.toISOString()}>
                {day.format('D')}
            </th>
        );

        const getDateCell = (row: IGanttRow, day: moment.Moment) => {
                        const isTaskOnDay =
                            day.dayOfYear() >= moment(row.start).dayOfYear() &&
                            day.dayOfYear() <= moment(row.end).dayOfYear();

                        // Sunday or Saturday, respectively
                        const isWeekend = day.weekday() === 0 || day.weekday() === 6;
                        const bgcolor = isWeekend
                            ? '#999'
                            : isTaskOnDay
                                ? row.resourceId
                                    ? stringToColor(row.resourceId)
                                    : 'black'
                                : 'transparent';

                        return (
                            <td key={day.toISOString()}
                                style={{ border: '1px solid grey', background: bgcolor }}
                                title={`${row.taskName} by ${row.resourceId} ends ${moment(row.end).format()}`} />
                        );
                    };

        const getDateRow = (row: IGanttRow) => (
            <tr key={row.taskId}>
                {days.map((day) => getDateCell(row, day))}
            </tr>
        );

        return (
            <table style={{ tableLayout: 'fixed' }}>
                <thead>
                    <tr>{firstDayOfEachWeek.map(getWeekHeader)}</tr>
                    <tr>{days.map(getDateHeader)}</tr>
                </thead>
                <tbody>
                    {rows.map(getDateRow)}
                </tbody>
            </table>
        );
    }

    private getGanttRow(task: ITask): IGanttRow {
        return {
            taskId: task.get('id').toString(),
            taskName: task.get('title'),
            start: task.get('start'),
            end: task.get('end'),
            percentComplete: null,
            resourceId: task.get('resources'),
        };
    }
}

const mapStateToProps = (state: any, ownProps: any): IStateProps => {
    // Pass immutable props, so connect() automatically optimizes shouldComponentUpdate()
    return {
        tasks: selectors.getTasksPreOrdered(state),
    };
};

const mapDispatchToProps = (dispatch: Redux.Dispatch<any>, ownProps: any): IDispatchProps => {
    return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(GanttChart);
