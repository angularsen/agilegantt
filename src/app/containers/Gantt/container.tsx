import * as React from 'react';
import * as Helmet from 'react-helmet';
import * as _ from 'underscore';
import TasksTable from './TasksTable/TasksTable';
import GanttChart from './GanttChart/GanttChart';

require('./style.sass');

let SplitPane = require('react-split-pane');

interface IState {
  initialSplitPos?: number;
}

class Gantt extends React.Component<any, IState> {
  constructor() {
    super();

    // Use default for server-side rendering, will be updated client-side a few moments later
    const defaultSplitPos = 500;
    let splitPos = typeof localStorage === 'undefined' || localStorage === null
      ? defaultSplitPos
      : parseInt(localStorage.getItem('splitPos'), 10);

    this.state = { initialSplitPos: splitPos };

    this.onSplitPaneChange = (size: number) => {
      this.saveSplitPosDebounced(size);
    };

    this.onSplitPaneChange.bind(this);
  }

  public render() {
    return (
      <div className="container">
        <Helmet title="Gantt" />
        <div className="wrapper">

          <SplitPane split="vertical"
            defaultSize={this.state.initialSplitPos || 200}
            onChange={this.onSplitPaneChange}>

            <div style={{ overflowX: 'auto', whiteSpace: 'nowrap' }}>
              <TasksTable />
            </div>
            <div>
              {/*<h2>Gantt chart</h2>*/}
               <GanttChart />
            </div>

          </SplitPane>

        </div>
      </div >
    );
  }

  private onSplitPaneChange: (size: number) => void;
  private saveSplitPosDebounced = _.debounce((splitPos: number) => {
    localStorage.setItem('splitPos', splitPos.toString());
  }, 500);

}

export { Gantt };
