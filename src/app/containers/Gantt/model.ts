import * as Immutable from 'immutable';

export type EditCursorPosition = 'start' | 'end';

export interface IIsEditingMap extends Immutable.Map<string, any> {
  get(key: 'taskId'): number;
  get(key: 'colName'): string;
  get(key: 'initialCursorPosition'): EditCursorPosition;
}

export interface IIsEditingJs {
    taskId: number;
    colName: string;
    initialCursorPosition: EditCursorPosition;
}

export interface IModel extends Immutable.Map<string, any> {
    get(key: 'root'): ITask;
    get(key: 'selectedTaskPath'): Immutable.Seq<string | number, string | number>;
    get(key: 'selectedColName'): string;
    get(key: 'cols'): Immutable.List<IColumnIm>;
    get(key: 'isEditing'): IIsEditingMap;
    get(key: 'isHydrated'): boolean;
    toJS(): IModelJs;
}

export interface IModelJs {
    root: ITaskJs;
    selectedTaskPath: Array<string | number>;
    selectedColName: string;
    cols: IColumnJs[];
    isEditing: { taskId: number, colName: string };
    isHydrated?: boolean;
}

export interface IStoredModel {
    root: ITaskJs;
}

export interface ITask extends Immutable.Map<string, any> {
    get(key: string): any;
    get(key: 'id'): number;
    get(key: 'wbs'): string;
    get(key: 'depth'): number;
    get(key: 'title'): string;
    get(key: 'work'): number;
    get(key: 'start'): Date;
    get(key: 'end'): Date;
    get(key: 'resources'): string;
    get(key: 'children'): Immutable.List<ITask>;
    set(key: 'id', val: number): ITask;
    set(key: 'start', val: Date): ITask;
    set(key: 'end', val: Date): ITask;
    set(key: 'children', val: Immutable.List<ITask>): ITask;
    toJS(): ITaskJs;
}

export interface ITaskJs {
    id: number;
    wbs: string;
    depth: number;
    title: string;

    /**
     * Number of hours estimated for this task, or the sum of its children's estimates if it has children.
     */
    work: number;

    start: Date;
    end: Date;
    resources: string;
    children: ITaskJs[];
    [key: string]: any;
}

export type ValueType  = 'string' | 'number';

export interface IColumnIm {
  get(key: 'name'): string;
  get(key: 'propName'): string;
  get(key: 'isReadOnly'): boolean | undefined;
  get(key: 'type'): ValueType;
}

export interface IColumnJs {
  name: string;
  propName: string;
  isReadOnly?: boolean;
  type: ValueType;
}
