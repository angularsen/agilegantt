import {EditCursorPosition} from './model';

export const ADD_BEFORE = 'tasks/ADD_BEFORE';
export const ADD_AFTER = 'tasks/ADD_AFTER';
export const REMOVE = 'tasks/REMOVE';

export const SET_PROPERTY = 'tasks/SET_PROPERTY';
export interface ISetPropertyPayload {
    value: any;
}

export const EDIT_PROPERTY = 'tasks/EDIT_PROPERTY';
export interface IEditPropertyPayload {
  cursorPosition: EditCursorPosition;
}

export const NAV_UP = 'tasks/NAV_UP';
export const NAV_DOWN = 'tasks/NAV_DOWN';
export const NAV_RIGHT = 'tasks/NAV_RIGHT';
export const NAV_LEFT = 'tasks/NAV_LEFT';
export const MOVE_UP = 'tasks/MOVE_UP';
export const MOVE_DOWN = 'tasks/MOVE_DOWN';
export const MOVE_RIGHT = 'tasks/MOVE_RIGHT';
export const MOVE_LEFT = 'tasks/MOVE_LEFT';
export const CANCEL_EDIT = 'tasks/CANCEL_EDIT';
