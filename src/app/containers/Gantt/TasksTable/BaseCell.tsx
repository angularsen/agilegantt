import * as React from 'react';

export interface IBaseCellProps {
  isSelected: boolean;
}

export interface IBaseCellState {

}

export abstract class BaseCell<TProps extends IBaseCellProps, TState extends IBaseCellState>
  extends React.Component<TProps, TState> {
  constructor(props: TProps) {
    super(props);

    if (this.props.isSelected) {
      // hotkey.addHandler(this.hotkeyHandler);
      // console.log('bind hotkey to TextCell');
    }
  }

  // private hotkeyHandler: any;

  public componentDidMount() {
    // hotkey.activate();
  }

  public componentDidUpdate(prevProps: IBaseCellProps, prevState: IBaseCellState) {
    if (!prevProps.isSelected && this.props.isSelected) {
      // hotkey.addHandler(this.hotkeyHandler);
      // console.log('bind hotkey to TextCell');
    } else if (prevProps.isSelected && !this.props.isSelected) {
      // hotkey.removeHandler(this.hotkeyHandler);
      // console.log('unbind hotkey to TextCell');
    }
  }
}
