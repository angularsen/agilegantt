import * as React from 'react';
import * as Immutable from 'immutable';
import * as Mousetrap from 'mousetrap';
// import { createSelector, createStructuredSelector } from 'reselect';

let {connect} = require('react-redux');

// import NodeUtils from '../../utils/NodeUtils';

import { NodeRow } from './NodeRow';
import { ITask, IIsEditingMap, IColumnIm, EditCursorPosition } from '../model';
import * as actions from '../actions';
import * as selectors from '../selectors';

require('./style.sass');

interface IStateProps {
  tasks: Immutable.List<ITask>;
  selectedColName: string;
  selectedId: number;
  cols: Immutable.List<IColumnIm>;
  isEditing: IIsEditingMap;
}

interface IDispatchProps {
  addBefore: () => void;
  addAfter: () => void;
  navUp: () => void;
  navDown: () => void;
  navLeft: () => void;
  navRight: () => void;
  moveUp: () => void;
  moveDown: () => void;
  moveLeft: () => void;
  moveRight: () => void;
  remove: () => void;
  setProperty: (propName: string, value: any) => void;
  editProperty: (cursorPosition: EditCursorPosition) => void;
  cancelEdit: () => void;
}

export interface ITasksTableProps extends IStateProps, IDispatchProps {

}

export class TasksTable extends React.Component<ITasksTableProps, {}> {
  constructor(props: ITasksTableProps) {
    super(props);

    this.onCellChange = this.onCellChange.bind(this);
    this.onCancelEdit = this.onCancelEdit.bind(this);

    const serverSideNoOpFunction = () => {
      // server-side no op
    };

    // TODO Use react-hotkey instead
    let mousetrapBind = typeof Mousetrap.bind === 'function' ? Mousetrap.bind : serverSideNoOpFunction;

    mousetrapBind(['up', 'k'], (e) => {
      e.preventDefault();
      props.navUp();
    });

    mousetrapBind(['down', 'j'], (e) => {
      e.preventDefault();
      props.navDown();
    });

    mousetrapBind(['right', 'e'], (e) => {
      e.preventDefault();
      props.navRight();
    });

    mousetrapBind(['left', 'n'], (e) => {
      e.preventDefault();
      props.navLeft();
    });

    mousetrapBind(['ctrl+up', 'K'], (e) => {
      e.preventDefault();
      props.moveUp();
    });

    mousetrapBind(['ctrl+down', 'J'], (e) => {
      e.preventDefault();
      props.moveDown();
    });

    mousetrapBind('ctrl+right', (e) => {
      e.preventDefault();
      props.moveRight();
    });

    mousetrapBind('ctrl+left', (e) => {
      e.preventDefault();
      props.moveLeft();
    });

    mousetrapBind(['i'], (e) => {
      e.preventDefault();
      props.editProperty('start');
    });

    mousetrapBind(['a', 'enter'], (e) => {
      e.preventDefault();
      props.editProperty('end');
    });

    mousetrapBind('O', (e) => {
      e.preventDefault();
      props.addBefore();
    });

    mousetrapBind('o', (e) => {
      e.preventDefault();
      props.addAfter();
    });

    mousetrapBind(['#', 'delete'], (e) => {
      e.preventDefault();
      props.remove();
    });
  }

  public render() {
    let {selectedColName, selectedId, cols, isEditing, tasks} = this.props;

    const rows = tasks.map((task) => {
      const isSelected = task.get('id') === selectedId;
      const initialCursorPosition = this.props.isEditing === undefined
        ? undefined
        : this.props.isEditing.get('initialCursorPosition');

      return (
        <NodeRow key={task.get('id')}
          isRowSelected={isSelected}
          task={task}
          selectedColName={selectedColName}
          cols={cols}
          onCellChange={this.onCellChange}
          onCancelEdit={this.onCancelEdit}
          initialIsEditing={isEditing !== undefined && isSelected}
          initialCursorPosition={initialCursorPosition}
          />
      );
    },
    );
    return (
      <table style={{ marginTop: '31px' }}>
        <thead>
          <tr className="blue">
            {cols.map((c) => <th key={c.get('propName')}>{c.get('name')}</th>)}
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table >
    );
  }

  private onCellChange(col: IColumnIm, task: ITask, value: any) {
    return this.props.setProperty(col.get('propName'), value);
  }

  private onCancelEdit() {
    return this.props.cancelEdit();
  }
}

const mapStateToProps = (state: any, ownProps: ITasksTableProps): IStateProps => {
  return {
    tasks: selectors.getTasksPreOrdered(state).skip(1).toList(), // skip root node
    selectedId: selectors.getSelectedTaskId(state),
    selectedColName: selectors.getSelectedColName(state),
    cols: selectors.getCols(state),
    isEditing: selectors.getIsEditing(state),
  };
};

const mapDispatchToProps = (dispatch: Redux.Dispatch<any>, ownProps: ITasksTableProps): IDispatchProps => {
  return {
    addBefore: () => dispatch(actions.addBefore()),
    addAfter: () => dispatch(actions.addAfter()),
    moveDown: () => dispatch(actions.moveDown()),
    moveLeft: () => dispatch(actions.moveLeft()),
    moveRight: () => dispatch(actions.moveRight()),
    moveUp: () => dispatch(actions.moveUp()),
    navDown: () => dispatch(actions.navDown()),
    navLeft: () => dispatch(actions.navLeft()),
    navRight: () => dispatch(actions.navRight()),
    navUp: () => dispatch(actions.navUp()),
    remove: () => dispatch(actions.remove()),
    setProperty: (propName: string, value: any) => dispatch(actions.setProperty(value)),
    editProperty: (cursorPosition: EditCursorPosition) => dispatch(actions.editProperty(cursorPosition)),
    cancelEdit: () => dispatch(actions.cancelEdit()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TasksTable);
