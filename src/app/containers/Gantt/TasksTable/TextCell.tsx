import * as React from 'react';
import * as classnames from 'classnames';
import { BaseCell } from './BaseCell';

// Safe to call multiple times, but could be moved out to single init
// let hotkey = require('react-hotkey');

export interface ITextCellProps {
  text: string;
  displayRenderer?: (text: string) => React.ReactElement<any>;
  editorRenderer?: (text: string) => React.ReactElement<any>;
  onChange: (text: string) => void;
  onCancelEdit: () => void;
  className?: string;
  isSelected: boolean;
  isReadOnly: boolean;
  initialIsEditing: boolean;
  initialCursorPosition: 'start' | 'end';
}

export interface ITextCellState {
  isEditing?: boolean;
  editorText?: string;
}

export class TextCell extends BaseCell<ITextCellProps, ITextCellState> {

  private inputComponent: HTMLInputElement;

  constructor(props: ITextCellProps) {
    super(props);
    this.state = { isEditing: props.initialIsEditing, editorText: props.text };
  }

  public componentWillReceiveProps(nextProps: ITextCellProps) {
    this.setState({ isEditing: nextProps.initialIsEditing }, () => {
      // Set initial cursor position
      if (nextProps.initialIsEditing &&
        nextProps.initialCursorPosition !== undefined &&
        this.inputComponent !== undefined) {

        switch (nextProps.initialCursorPosition) {
          case 'start':
            this.inputComponent.setSelectionRange(0, 0);
            break;
          case 'end':
            const endPos = this.inputComponent.value.length;
            this.inputComponent.setSelectionRange(endPos, endPos);
            break;
          default:
            throw Error('Unknown cursor position: ' + nextProps.initialCursorPosition);
        }
      }
    });
  }

  protected getDisplayComp(): React.ReactElement<any> {
    let { displayRenderer, text } = this.props;

    let content = displayRenderer
      ? displayRenderer(text)
      : (<div>{text}</div>);

    return <div>{content}</div>;
  }

  protected getEditorComp(): React.ReactElement<any> {
    let { editorText } = this.state;
    let { editorRenderer } = this.props;

    const inputStyle: React.CSSProperties = {
      width: '100%',
      padding: '5px',
      margin: 0,
      WebkitBoxSizing: 'border-box',
      MozBoxSizing: 'border-box',
      OBoxSizing: 'border-box',
      msBoxSizing: 'border-box',
      boxSizing: 'border-box',
    };

    return editorRenderer
      ? editorRenderer(editorText)
      : (
        <input ref={this.onInputRef.bind(this)} type="text" value={editorText}
          style={inputStyle}
          onChange={this.editorOnChange.bind(this)}
          onKeyDown={this.editorOnKeyDown.bind(this)} autoFocus />
      );
  }

  private onInputRef(c: HTMLInputElement): any {
    this.inputComponent = c;
  }

  private editorOnChange(e: React.FormEvent<HTMLInputElement>) {
    this.setState({ editorText: (e.target as any).value });
  }

  private editorOnKeyDown(e: React.KeyboardEvent<HTMLInputElement>) {
    let keyCodes = { enter: 13, esc: 27 };

    switch (e.keyCode) {
      case keyCodes.enter:
        this.props.onChange(this.state.editorText);
        break;
      case keyCodes.esc:
        this.props.onCancelEdit();
        break;
      default:
        break;
    }
  }

  public render() {
    let classname = classnames('TextCell', { readonly: this.props.isReadOnly });

    const content =
      this.state.isEditing && !this.props.isReadOnly
        ? this.getEditorComp()
        : this.getDisplayComp();

    return (<div className={classname}>{content}</div>);
  }
}
