import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as classnames from 'classnames';
import * as _ from 'underscore';
import * as Immutable from 'immutable';

import { ITask, IColumnIm, EditCursorPosition } from '../model';
import { TextCell } from './TextCell';

let scrollIntoView = require('scroll-into-view');

export interface INodeRowProps {
  initialIsEditing: boolean;
  initialCursorPosition?: EditCursorPosition;
  task: ITask;
  isRowSelected: boolean;
  selectedColName: string;
  cols: Immutable.List<IColumnIm>;
  onCellChange: (c: IColumnIm, task: ITask, value: any) => void;
  onCancelEdit: () => void;
}

export interface INodeRowState {
  task: ITask;
}

export class NodeRow extends React.Component<INodeRowProps, INodeRowState> {
  constructor(props: INodeRowProps) {
    super(props);

    this.onCellChange = this.onCellChange.bind(this);
    this.onCancelEdit = this.onCancelEdit.bind(this);
    this.state = { task: props.task };
  }

  public componentDidUpdate() {
    if (this.props.isRowSelected) {
      let domNode: any = ReactDOM.findDOMNode(this);
      if (!this.isScrolledIntoView(domNode)) {
        scrollIntoView(domNode, { time: 200 });
        // console.log(`Scrolling into view ${this.props.task.Title}`)
      }
    }
  }

  public render() {
    let {task, cols} = this.props;
    let depth = task.get('depth');
    let children = task.get('children');

    let rowClassName = classnames(
      'row',
      `row-level${depth}`,
      { 'active-row': this.props.isRowSelected },
      { parent: !children.isEmpty() },
    );

    return (
      <tr className={rowClassName}>
        {cols.map((c) => this.renderCell(c))}
      </tr>
    );
  }

  private renderCell(col: IColumnIm): React.ReactElement<any> {
    let {task, selectedColName, isRowSelected} = this.props;
    let colName = col.get('name');
    let isCellSelected = selectedColName === colName && isRowSelected;
    let cellClassName = classnames(
      { 'active-col': isCellSelected },
    );

    let isReadOnly: boolean = col.get('isReadOnly');
    let cellValue = task.get(col.get('propName'));
    let displayRenderer = (text: string) => <span>{cellValue}</span>;
    let style = {};
    if (colName === 'Title') {
      _.extend(style, { width: '100%' });
      displayRenderer = (text: string) => this.titleDisplayRenderer(task, text);
    }

    return (
      <td key={col.get('propName')} className={cellClassName} style={style}>
        <TextCell
          isSelected={isCellSelected}
          text={cellValue}
          displayRenderer={displayRenderer}
          onChange={this.onCellChange}
          onCancelEdit={this.onCancelEdit}
          isReadOnly={isReadOnly}
          initialIsEditing={this.props.initialIsEditing && isCellSelected}
          initialCursorPosition={this.props.initialCursorPosition}
          />
      </td>
    );
  }

  private onCellChange(text: string) {
    const c = this.props.cols.find((c) => c.get('name') === this.props.selectedColName);
    const task = this.props.task;
    this.props.onCellChange(c, task, text);
  }

  private onCancelEdit() {
    this.props.onCancelEdit();
  }

  private titleDisplayRenderer(task: ITask, text: string) {
    // Empty whitespace icon if no children
    const prefixIcon = task.get('children').count() === 0
      ? (<i className="fa fa-fw" />)
      : (<i className="fa fa-chevron-down" />);

    return (
      <div style={{ marginLeft: task.get('depth') + 'em' }}>{prefixIcon} {task.get('title')}</div>
    );
  }

  private isScrolledIntoView(el: Element) {
    const elemTop = el.getBoundingClientRect().top;
    const elemBottom = el.getBoundingClientRect().bottom;
    const isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
    return isVisible;
  }

}
