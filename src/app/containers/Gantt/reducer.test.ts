import { fromJS } from 'immutable';

import * as chai from 'chai';
chai.use(require('chai-moment'));
chai.use(require('chai-datetime'));
const { expect, assert } = chai;

import { } from 'chai-datetime';
import * as moment from 'moment';
import { reducer } from './reducer';
import { IModelJs, ITaskJs } from './model';
import NodeUtils from '../../utils/NodeUtils';

/**
 * Use this action to simulate how redux call calls the reducer with the initial state.
 */
const actionReduxInit = { type: '@@redux/INIT' };

// TODO Move me to a typings file
// tslint:disable-next-line:no-namespace
declare global {
  namespace Chai {
    type momentInput = string | Date | number | Object | any[] | moment.Moment;
    // tslint:disable-next-line:interface-name
    interface AssertStatic {
      sameMoment(left: momentInput, right: momentInput, message?: string): Assertion;
      beforeMoment(left: momentInput, right: momentInput, message?: string): Assertion;
      afterMoment(left: momentInput, right: momentInput, message?: string): Assertion;
    }
  }
}

let idCnt = 0;

function createTask(
  title: string,
  work: number,
  children: ITaskJs[] = [],
  resources: string = 'Supreme Coder'): ITaskJs {
  return {
    title, work, resources, id: idCnt++, children,
    depth: 0, end: undefined, start: undefined, wbs: undefined,
  };
}

function createTaskFor(
  resources: string,
  title: string,
  work: number,
  children: ITaskJs[] = []): ITaskJs {
  return {
    title, work, resources, id: idCnt++, children,
    depth: 0, end: undefined, start: undefined, wbs: undefined,
  };
}

describe('Gantt:reducer', () => {
  it('Schedules tasks sequentially in original order', () => {
    const initialState = <IModelJs> {
      cols: [],
      isEditing: undefined,
      selectedColName: undefined,
      selectedTaskPath: undefined,
      root: createTask('root', 0, [
        createTask('Task1', 1),
        createTask('Task2', 2),
      ]),
    };
    const newState = reducer(fromJS(initialState), actionReduxInit).toJS();
    expect(newState).to.have.deep.property('root.children.length', 2);
    expect(newState).to.have.deep.property('root.work', 3);
  });
});

describe('Gantt:reducer', () => {
  it('Starts initial tasks at root\'s start time', () => {
    const initialState = <IModelJs> {
      cols: [],
      isEditing: undefined,
      selectedColName: undefined,
      selectedTaskPath: undefined,
      root: createTask('root', 0, [
        createTaskFor('Guy1', 'Guy1_Task1', 3),
        createTaskFor('Guy2', 'Guy2_Task1', 16),
      ]),
    };
    // Start on a Monday at 08:00
    const start = moment('2017-01-02T08:00Z');
    initialState.root.start = start.toDate();

    // Act
    const newState = reducer(fromJS(initialState), actionReduxInit).toJS();

    // Assert
    assert.sameMoment(newState.root.start, start, 'root start should not change');
    assert.sameMoment(newState.root.children[0].start, start, 'Guy1 initial task start at root');
    assert.sameMoment(newState.root.children[1].start, start, 'Guy2 initial task start at root');
  });
});

// Start on Friday morning, schedule 16 hours (two days) of work over the weekend
// to end up at end of Monday
describe('Gantt:reducer', () => {
  it('Skips Saturday and Sunday', () => {
    const initialState = <IModelJs> {
      cols: [],
      isEditing: undefined,
      selectedColName: undefined,
      selectedTaskPath: undefined,
      root: createTask('root', 0, [
        createTask('Task1', 16),
      ]),
    };
    const start = moment('2017-01-06T08:00');
    initialState.root.start = start.toDate();

    // Act
    const newState = reducer(fromJS(initialState), actionReduxInit).toJS();

    // Assert
    // 8 hours on Friday, Saturday/Sunday skipped, then 8 hours on Monday
    assert.sameMoment(newState.root.end, moment('2017-01-09T16:00'), 'root end');
  });
});

describe('Gantt:reducer', () => {
  it('Schedules up to 8 hours per date per resource (TODO configurable)', () => {
    const initialState = <IModelJs> {
      cols: [],
      isEditing: undefined,
      selectedColName: undefined,
      selectedTaskPath: undefined,
      root: createTask('root', 0, [
        createTaskFor('Guy1', 'Guy1_Task1', 0), // 0 hrs, ends on Mon 08:00
        createTaskFor('Guy2', 'Guy2_Task1', 7), // 7 hrs, ends on Mon 15:00
        createTaskFor('Guy3', 'Guy3_Task1', 8), // 8 hrs, ends on Mon 16:00
        createTaskFor('Guy4', 'Guy4_Task1', 15), // 15 hrs, ends on Tue 15:00
        createTaskFor('Guy4', 'Guy4_Task2', 15), // 15+15 hrs, ends on Thu 14:00
      ]),
    };
    // Start on a Monday at 08:00
    initialState.root.start = moment('2017-01-02T08:00').toDate();

    // Act
    const newState = reducer(fromJS(initialState), actionReduxInit).toJS();

    // Assert
    const taskSummaries = newState.root.children.map((task) => {
      const format = `ddd MMM D HH:mm`;
      const start = moment(task.start).format(format);
      const end = moment(task.end).format(format);
      return `${task.title} [${start}] to [${end}]`;
    });

    const expectedSummaries = [
      'Guy1_Task1 [Mon Jan 2 08:00] to [Mon Jan 2 08:00]',
      'Guy2_Task1 [Mon Jan 2 08:00] to [Mon Jan 2 15:00]',
      'Guy3_Task1 [Mon Jan 2 08:00] to [Mon Jan 2 16:00]',
      'Guy4_Task1 [Mon Jan 2 08:00] to [Tue Jan 3 15:00]',
      'Guy4_Task2 [Tue Jan 3 15:00] to [Thu Jan 5 14:00]',
    ];

    taskSummaries.forEach((summary, i) => {
      assert.equal(summary, expectedSummaries[i]);
    });
  });
});

describe('Gantt:reducer', () => {
  it('Parent work calculated from sum of children\'s work', () => {
    const initialState = <IModelJs> {
      cols: [],
      isEditing: undefined,
      selectedColName: undefined,
      selectedTaskPath: undefined,
      root: createTask('root', undefined, [ // 15 + 20 = 35
        createTask('Task1', undefined, [ // 10 + 5 = 15
          createTask('Task1.1', 10),
          createTask('Task1.2', 5),
        ]),
        createTask('Task2', undefined, [ // 20 (same as only child)
          createTask('Task2.1', 20),
        ]),
      ]),
    };
    // Start on a Monday at 08:00
    initialState.root.start = moment('2017-01-02T08:00').toDate();

    // Act
    const newState = reducer(fromJS(initialState), actionReduxInit).toJS();

    // Assert
    const tasks = NodeUtils.traverseDfsPreOrder(newState.root, (task) => task.children);
    const workSummaries = tasks.map((task) => `${task.title}(${task.work})`);

    const expectedSummaries = [
      'root(35)',
      'Task1(15)',
      'Task1.1(10)',
      'Task1.2(5)',
      'Task2(20)',
      'Task2.1(20)',
    ];

    workSummaries.forEach((summary, i) => {
      assert.equal(summary, expectedSummaries[i]);
    });
  });
});
