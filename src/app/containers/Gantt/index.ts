import * as model from './model';
import * as actions from './actions';
import * as constants from './constants';
import * as container from './container';
import {reducer} from './reducer';
import * as selectors from './selectors';

export default { actions, constants, container, model, reducer, selectors };
