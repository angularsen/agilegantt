import * as _ from 'underscore';
import * as moment from 'moment';

import { ITaskJs } from './model';

let input = `Name|Work|Predecessors|Resources
Apply for TradeMark Motion Catalyst|8h||Kristian
Source logo mats in the US with new color|8h||Tom
Export data (force channels, COP, foot positions)|24h||Andreas
MC Website and Online|44h
    Trial and purchase info|20h||Ståle
    Rebrand Online to SC/MC/Bertec|24h||Andreas
Laptop package|128h
    Research and order laptops/cams|32h||Stian,Øystein
    Waiting on orders|0h|8
    Test combinations of laptop + cam + BP|80h|9|Stian,Øystein
    Put laptop package on webshop|8h|10|Ståle
    Add/update support articles on supported cameras and PCs|8h|10|Øystein
Dev MC 7.1
    New lesson to fix issues with certain PCs|120h||Øystein
Portable trolley for desktop/laptop|88h
    Research trolley options|16h||Ståle
    Wait for trolley order|0h|14
    Test and finalize trolley setup with desktop/laptop|16h|15|Ståle
    Publish trolley on webshop|8h|16|Ståle
    Publish trolley on product pages|8h|16|Ståle
    Create trolley video|40h|16|Ståle
New vertical pilots|72h
    Establish 1+ athletics pilots|24h||Erlend
    Establish 1+ vertical jump pilots|24h||Kristian
    Establish 1+ workout/gym pilots|24h||Kristian
Require activation online to use BP (warranty)|32h
    API to register devices by type, S/N and email, manual follow up|16h||Andreas
    Impl activation in desktop|16h||Erlend
ESA for Balance Plate
    Routine for ESA statements|16h||Andreas
    Routine for monthly ESA reports to SensorEdge|8h||Andreas
ESA for Motion Plate
    Obtain terms from Bertec|8h||Kristian[10%]
    Determine pricing|16h|31|Kristian,Tom
    Determine routines for signing up|8h|32|Andreas
    Publish information to distributors|8h|33|Kristian
Dev MC 7.1
    Export contact information|8h||Erlend
    Configurable recording duration (30 s)|80h
        Impl|80h||Andreas
    Access to settings from analysis view|40h
        UI|16h||Ståle
        Impl|24h|40|Erlend
    Performance: Faster graph databoxes|24h||Andreas
Stabilize MC 7.1|120h|35
    Test/fix|40h||Andreas
    Test/fix|40h||Erlend
    Test/fix|40h||Øystein`;

let lines = input.split('\n');
let parsedInput = _.chain(lines).last(lines.length - 1).map((line) => {
  let cols = line.split('|');
  let [title, work, predecessors, resources] = cols;
  return { title, work, predecessors, resources };
}).value();

let wbsCounter: number[] = [];
let parentStack: ITaskJs[] = [];

let rootTask: ITaskJs = {
  id: -1,
  wbs: null,
  title: '(root)',
  depth: -1,
  work: -1,
  start: moment().hour(0).toDate(),
  end: moment().hour(0).toDate(),
  resources: null,
  children: [],
};

// debugger

let prevTask: ITaskJs = rootTask;
// let parsedTasks: Array<ITask> =
parsedInput.map((e, i): ITaskJs => {
  let depth = e.title.search(/\S/); // Indent space count equals index of first non-whitespace char
  depth = depth / 4; // 4 spaces per indent level

  // console.log(`depth: ${depth} - ${e.title}`);

  // Initialize or increment WBS counter at depth
  wbsCounter[depth] = wbsCounter[depth] === undefined ? 1 : wbsCounter[depth] + 1;

  // Reset counters deeper than depth
  for (let di = depth + 1; di < wbsCounter.length; di++) {
    wbsCounter[di] = 0;
  }

  let wbs: string = wbsCounter.slice(0, 1 + depth).join('.');

  if (depth < prevTask.depth) {
    parentStack.pop();
  } else if (depth > prevTask.depth) {
    parentStack.push(prevTask);
  }

  let parent = parentStack.length === 0 ? null : _.last(parentStack);
  let workHours = e.work ? parseInt(e.work.substring(0, e.work.length - 1), 10) : 0;

  // debugger

  let entry: ITaskJs = {
    id: i,
    wbs,
    depth,
    title: e.title.trim(),
    work: workHours,
    start: null,
    end: null,
    resources: e.resources ? e.resources.split(',', 1)[0] : null, // TODO Support multi-resoucre
    children: [],
  };

  if (parent) {
    parent.children = parent.children.concat(entry);
  }
  prevTask = entry;
  return entry;
});

export let root = rootTask;
