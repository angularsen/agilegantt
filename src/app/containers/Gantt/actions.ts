import * as t from './actionTypes';
import {EditCursorPosition} from './model';

export const addBefore = () => ({
  type: t.ADD_BEFORE,
});

export const addAfter = () => ({
  type: t.ADD_AFTER,
});

export const remove = () => ({
  type: t.REMOVE,
});

export const moveUp = () => ({
  type: t.MOVE_UP,
});

export const moveDown = () => ({
  type: t.MOVE_DOWN,
});

export const moveRight = () => ({
  type: t.MOVE_RIGHT,
});

export const moveLeft = () => ({
  type: t.MOVE_LEFT,
});

export const navUp = () => ({
  type: t.NAV_UP,
});

export const navDown = () => ({
  type: t.NAV_DOWN,
});

export const navRight = () => ({
  type: t.NAV_RIGHT,
});

export const navLeft = () => ({
  type: t.NAV_LEFT,
});

export const setProperty = (value: string) => ({
  type: t.SET_PROPERTY,
  payload: {
    value,
  },
});

export const editProperty = (cursorPosition: EditCursorPosition) => ({
  type: t.EDIT_PROPERTY,
  payload: { cursorPosition },
});

export const cancelEdit = () => ({
  type: t.CANCEL_EDIT,
});
