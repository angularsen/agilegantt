const appConfig = require('../../../../config/main');
import * as React from 'react';
import * as Helmet from 'react-helmet';
import { Header } from 'components';
let {connect} = require('react-redux');

class App extends React.Component<any, any> {
  public render() {
    const s = require('./style.sass');
    const storeToken = localStorage.getItem('storeToken');
    const url = (location.protocol + '//' + location.host + location.hash + `&storeToken=${storeToken}`);

    return (
      <section className={s.AppContainer}>
        <Helmet {...appConfig.app} {...appConfig.app.head}/>
        <Header />
        {/* TODO using localStorage seems hackish, can we use props via state or route instead? */}
        <div><a href={url}>{url}</a></div>
        {this.props.children}
      </section>
    );
  }
}

const mapStateToProps = (state: any, ownProps: any): any => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Redux.Dispatch<any>, ownProps: any): any => {
  return {

  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
