import * as React from 'react';
const s = require('./style.css');

class Home extends React.Component<any, any> {
  public render() {
    return (
      <div className={s.home}>
        <img src={require('./agile-att.jpg')} />
        <p>
          Gantt for agile teams, where the roadmap and priorities changes frequently
          and you need to quickly are out how to best employ your resources.
        </p>
      </div>
    );
  }
}

export { Home }
