require('babel-polyfill');

import { expect } from 'chai';
import { renderComponent } from 'helpers/TestHelper';
import { Home } from './index';
import 'chai-dom';
chai.use(require('chai-dom'));

describe('<Home />', () => {

  const component = renderComponent(Home);

  it('Renders with correct style', () => {
    const s = require('./style.css');
    expect(component.find(s.home)).to.exist;
  });

  it('Renders agile-att ', () => {
    const img = component.find('img');
    expect(img).to.exist;

    // The below throws an exception:
    // undefined is not a constructor (evaluating 'chai_1.expect(document.querySelector('img')).to.have.attr('src')')
    // expect(img).to.have.attr('src', '/images/agile-att.jpg');
    // expect(document.querySelector('img')).to.have.attr('src').match('/public/');
  });

  it('Has a p element with introduction text.', () => {
    expect(component.find('p').text())
      .contains('Gantt for agile teams, where the roadmap and priorities changes' +
      ' frequently and you need to quickly are out how to best employ your resources.');
  });

});
