import * as React from 'react';
import { Link } from 'react-router';

class Header extends React.Component<any, any> {
  public render() {
    const s = require('./style.sass');

    return (
      <nav className={s.nav}>
        <ul>
          <li><Link to={{ pathname: '/' }}>Home</Link></li>
          <li><Link to={{ pathname: 'gantt' }}>Gantt</Link></li>
          <li><Link to={{ pathname: 'about' }}>About</Link></li>
        </ul>
      </nav>
    );
  }
}

export { Header }
