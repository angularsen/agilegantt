import * as _ from 'underscore';

function traverseDfsPreOrder<T>(node: T, getChildren: (n: T) => T[]): T[] {
  return _.chain([node])
    .concat(_.flatten(getChildren(node)
      .map((child) => traverseDfsPreOrder(child, getChildren)),
    )).value();
}

/** Traverse nodes depth first, from leaf to root. */
function traverseDfsPostOrder<T>(node: T, getChildren: (n: T) => T[]): T[] {
  return _.chain(getChildren(node))
    .map((child) => traverseDfsPostOrder(child, getChildren))
    .flatten()
    .concat([node])
    .value();
}

export default {
  traverseDfsPreOrder,
  traverseDfsPostOrder,
};
