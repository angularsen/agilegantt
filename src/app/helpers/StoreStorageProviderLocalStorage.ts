import { Store } from 'redux';
import { fromJS } from 'immutable';

import { IStore, IStoreJs } from '../redux/IStore';
import {
  IStoredModel as IGanttStoredModel,
} from '../containers/Gantt/model';

// const keys = {
//   state: 'state',
// };

const storageKey = 'agilegantt';

interface IStoredState {
  gantt: IGanttStoredModel;
}

interface IOptions {
  token?: string;
}

export class Storage {
  private opts: IOptions;
  private storageUrl: string;

  constructor(opts: IOptions) {
    if (!opts) { throw new Error('Options must be set.'); }
    this.opts = opts;

    if (opts.token) {
      this.storageUrl = this.getStorageUrl(opts.token);
    }
  }

  /**
   * Saves store to LocalStorage when it changes.
   * @param store Store to subscribe to.
   */
  public bind = (store: Store<IStore>): void => {
    store.subscribe(() => {
      this.save(this.toStoredState(store.getState()));
    });
  }

  public save = (state: IStoredState): Promise<any> => {
    return this.getOrCreateStorageUrl()
      .then((url) => {
        // Save all state by default, remove props to opt-out
        // localStorage.setItem(keys.state, JSON.stringify(state));

        const encodedState = encodeURIComponent(JSON.stringify(state));
        return fetch(`${url}/${encodedState}`, {
          method: 'POST',
        });

        // return fetch(url, {
        //   method: 'POST',
        //   body: JSON.stringify(state),
        //   headers: {
        //     'Accept': 'application/json',
        //     'Content-Type': 'application/json',
        //   },
        // });
      })
      .then((res) => {
        if (res.status === 200) {
          console.debug('Saved OK.');
        } else {
          console.warn('Failed to save.', res);
        }
      })
      .catch((reason) => {
        console.error('Failed to save.', reason);
      });
  }

  public load = (): Promise<IStore> => {
    return this.getOrCreateStorageUrl()
      .then(fetch)
      .then((res) => {
        if (res.status === 200) {
          return res.text();
        }
        throw new Error(`Request failed, HTTP status: ${res.status} ${res.statusText}`);
      })
      .then((text) => {
        if (!text || text.trim().length === 0) { return undefined; }
        return JSON.parse(decodeURIComponent(text)) as IStoredState;
      })
      .then((storedState) => {
        if (!storedState) { return undefined; }
        console.info('Loaded state.');
        return <IStore> fromJS(<IStoreJs> {
          // Convert JSON to Immutable JS for modules expecting immutable types
          gantt: storedState.gantt ? fromJS(storedState.gantt) : undefined,
        });
      })
      .catch((err) => {
        console.error('Unable to load stored state.', err);
        return undefined;
      });
  }

  private toStoredState(state: IStore): IStoredState {
    return {
      gantt: {
        root: state.get('gantt').get('root').toJS(),
      },
    };
  }

  private getStorageUrl(token: string): string {
    return `https://api.keyvalue.xyz/${token}/${storageKey}`;
  }

  private getOrCreateStorageUrl(): Promise<string> {
    return this.storageUrl
      ? Promise.resolve(this.storageUrl)
      : this.createStorageUrl();
  }

  private createStorageUrl(): Promise<string> {
    console.debug('Creating storage URL...');
    return fetch(`https://api.keyvalue.xyz/new/${storageKey}`, { method: 'POST' })
      .then((res) => res.text())
      .then((url) => {
        this.storageUrl = url;
        console.info('Created storage URL: ' + url);
        return url;
      });
  }

}
