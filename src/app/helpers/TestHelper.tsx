/** React Specific */
import * as React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from 'redux/reducers';
import configureStore from 'redux-mock-store';

const fetchMock = require('fetch-mock');

/** Redux Mock Store Configuration */
import thunk from 'redux-thunk';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

/** Render Component */
function renderComponent(ComponentClass, state?, props?) {
  const store = createStore(rootReducer, state);

  return mount((
    <Provider store={store}>
      <ComponentClass {...props} />
    </Provider>
  ),
  );
}

const addChaiDateAssertion = () => {
  (chai as any).Assertion.addChainableMethod('equalTime', (time) => {
    const expected = time.getTime();
    const actual = this._obj.getTime();

    return this.assert(
      actual === expected,
      'expected ' + this._obj + ' to equal ' + time,
      'expected ' + this._obj + ' to not equal ' + time,
    );
  });

  (chai as any).Assertion.addChainableMethod('equalDate', (date) => {
    const expectedDate = date.toDateString();
    const actualDate = this._obj.toDateString();

    return this.assert(
      expectedDate === actualDate,
      'expected ' + actualDate + ' to equal' + expectedDate,
      'expected ' + actualDate + ' to not equal' + expectedDate,
    );
  });
};

export { mockStore, fetchMock, renderComponent, addChaiDateAssertion };
