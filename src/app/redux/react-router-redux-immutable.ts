// Workaround for using react-router-redux with redux-immutable,
// setting state using Immutable.js objects and operations.
// https://github.com/gajus/redux-immutable#using-with-react-router-redux
import * as Immutable from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';

const initialState = Immutable.fromJS({
  locationBeforeTransitions: null,
});

export default (state = initialState, action) => {
  if (action.type === LOCATION_CHANGE) {
    return state.set('locationBeforeTransitions', action.payload);
  }

  return state;
};
