import { combineReducers } from 'redux-immutable';
// import { routerReducer } from 'react-router-redux';
import routerReducer from './react-router-redux-immutable';
import { IStore } from './IStore';
import gantt from '../containers/Gantt';

// const { reducer } = require('redux-connect');

const rootReducer: Redux.Reducer<IStore> = combineReducers<IStore>({
  routing: routerReducer,
  // reduxAsyncConnect: reducer,
  [gantt.constants.NAME]: gantt.reducer,
});

export default rootReducer;
