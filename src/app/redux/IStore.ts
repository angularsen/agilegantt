import { Map } from 'immutable';
import { IModel as IGantt, IModelJs as IGanttJs } from '../containers/Gantt/model';

export interface IStore extends Map<string, any> {
  get(key: 'gantt'): IGantt;
  get(key: 'storeToken'): string;
  set(key: 'gantt', value: IGantt);
  set(key: 'storeToken', value: string);
};

export interface IStoreJs {
  gantt: IGanttJs;
};
