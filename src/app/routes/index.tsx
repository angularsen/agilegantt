import * as React from 'react';
import { IndexRoute, Route } from 'react-router';
import * as containers from '../containers';

export default (
  <Route path="/" component={containers.App}>
    <IndexRoute component={containers.Home} />
    <Route path="gantt" component={containers.Gantt} />
    <Route path="about" component={containers.About} />
  </Route>
);
