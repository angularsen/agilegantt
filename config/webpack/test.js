require('es6-promise').polyfill();
require('isomorphic-fetch');
require('babel-polyfill'); // Required for regeneratorRuntime, used by immutable-treeutils

var context = require.context('../../src', true, /.test\.tsx?$/);
context.keys().forEach(context);
